package gnu.math;
import java.io.*;

public class UByte extends UnsignedPrim
    implements Comparable<UByte>, Externalizable {
    byte ival;
    public int numBits() { return 8; }

    public UByte(byte ival) { this.ival = ival; }

    public static UByte valueOf(byte ival) { return new UByte(ival); }

    public int intValue() { return ival & 0xFF; }

    public IntNum toIntNum() { return IntNum.valueOf(ival & 0xFF); }

    public boolean equals(Object obj) {
        return obj instanceof UByte
            && ival == ((UByte) obj).ival;
    }

    public int compareTo(UByte other) {
        return intValue() - other.intValue();
    }

    public void writeExternal(ObjectOutput out) throws IOException {
	out.writeByte(ival);
    }

    public void readExternal(ObjectInput in)
	throws IOException, ClassNotFoundException {
	ival = in.readByte();
    }

    public static String toString(byte ival) {
        return Integer.toString(ival & 0xFF);
    }
    public String toString() { return toString(ival); }
}
